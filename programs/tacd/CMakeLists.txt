add_executable( tacd main.cpp )

if( TAC_STATIC_BUILD )
  target_link_libraries( tacd PRIVATE
                        "-static-libstdc++ -static-libgcc"
                        tac_chain tac_legal
  )
else()
  target_link_libraries( tacd PRIVATE
                        tac_chain tac_legal
  )
endif()

install( TARGETS
  tacd

  RUNTIME DESTINATION bin
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib
)