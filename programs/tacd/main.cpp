#include <iostream> // For cout
#include <chrono> // For now
#include <thread>
#include <exception>

#include <cpptools/logger.hpp>
#include <cpptools/exception.hpp>

#include <tac/chain/config.hpp>
#include <tac/chain/blockchain.hpp> // For blockchain

#include <tac/legal/copyright.hpp>

namespace
{
  using namespace tac::chain;

  void display_block( const block_t& _b )
  {
    cpptools_dlog
      << "nonce:      " << _b.nonce                           << cpptools::fnl
      << "timestamp:  " << _b.timestamp                       << cpptools::fnl
      << "previous:   " << _b.previous.operator std::string() << cpptools::fnl
      << "digest:     " << _b.digest().operator std::string() << cpptools::fnl
      << "difficulty: " << _b.get_difficulty()                << cpptools::endl;
  }
}

int main()
{
  try
  {
    tac::legal::copyright::display_notice();

    // TODO: Boost program options

    cpptools_ilog << "Starting server..." << cpptools::endl;

    // TODO: Chain id
    blockchain bc;
    bc.mine();

    cpptools_dlog << "Block 1:" << cpptools::endl;
    display_block( bc.get_block( 1 ) );

    // TODO: Move to tests:
    block::transfer_operation null_op;
    null_op.memo = "Hello, world!";
    null_op.amount.amount = 1;
    block::signed_transaction trx;
    trx.operations.emplace_back( null_op );
    const auto now = std::chrono::system_clock::now();
    trx.expiration = std::chrono::duration_cast< std::chrono::seconds >( now.time_since_epoch() ).count() + 10;

    block::block _b;
    _b.previous = bc.get_block( 1 ).digest();
    _b.transactions.push_back( trx );

    bc.add( _b );
    cpptools_ilog << "Sleeping for " << TAC_MIN_BLOCK_PRODUCTION_GAP << " seconds to produce another block" << cpptools::endl;
    std::this_thread::sleep_for(std::chrono::seconds(TAC_MIN_BLOCK_PRODUCTION_GAP)); // Wait to produce another block
    bc.mine();

    cpptools_dlog << "Block 2:" << cpptools::endl;
    display_block( bc.get_block( 2 ) );

    bc.validate();
  }
  CPPTOOLS_CATCH_LOG();

  return 0;
}