# TAC
Throw-Away Coin - My blockchain implementation created in C++

## Dependencies
* C++23 (GCC-11+ compiler for consteval and <=> operator support)
* CMake
* Supports Linux, Mac OS X

## Building
### Building on Ubuntu 18.04/20.04
For Ubuntu 18.04/20.04 users, after installing the right packages with apt, tacd's programs will build out of the box without further effort:
```bash
sudo apt-get update

sudo apt-get install -y \
    cmake \
    g++ \
    git \
    make

git clone https://gitlab.com/mtyszczak/TAC.git
cd TAC
git checkout master
git submodule update --init --recursive --progress
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j$(nproc) all
# optional
make install  # defaults to /usr/local
```

> This shall generate the binary executable in the build directory that can be run to verify that the make file indeed compiles the project

## License
See [LICENSE.md](LICENSE.md)