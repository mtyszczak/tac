#include <tac/legal/copyright.hpp>

#include <iostream>

namespace tac { namespace legal {

  void copyright::display_notice()
  {
    std::cout
      << "TAC  Copyright (C) 2021  Mateusz Tyszczak\n"
      << "This program comes with ABSOLUTELY NO WARRANTY; This is free\n"
      << "software, and you are welcome to redistribute it under certain conditions"
      << std::endl;
  }

} } // tac::legal
