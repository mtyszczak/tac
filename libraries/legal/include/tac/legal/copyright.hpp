#pragma once

namespace tac { namespace legal {

  class copyright
  {
  public:
    static void display_notice();
  };

} } // tac::legal
