#include <tac/chain/block/extensions/base_extensions.hpp>

#include <chrono>

namespace tac { namespace chain { namespace block { namespace extensions {

  void created_at_extension::use_now()
  {
    const auto now = std::chrono::system_clock::now();
    time_since_epoch = std::chrono::duration_cast< std::chrono::seconds >( now.time_since_epoch() ).count();
  }

} } } } // tac::chain::block::extensions
