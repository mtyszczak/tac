#include <tac/chain/block/block.hpp>

#include <tac/chain/types.hpp>

#include <cpptools/streamable.hpp>
#include <cpptools/math.hpp>

#include <cpptools/exception.hpp>

#include <tac/chain/config.hpp>

#include <chrono>

namespace tac { namespace chain { namespace block {

  namespace
  {
    using cpptools::SHA256::encoder;
    using cpptools::null::write_counter;
  }

  hash_t block::digest()const
  {
    encoder sha;
    pack( sha, *this );
    return hash_t{ sha.digest(), 32 };
  }

  mining_difficulty_t block::get_difficulty()const
  {
    write_counter c;
    pack( c, *this );
    return c.get_total() * TAC_MAX_BLOCK_SIZE_MULTIPLIER;
  }

  bool block::is_valid_mine( mining_difficulty_t mining_difficulty )const
  {
    auto digest_data = digest();
    CPPTOOLS_ASSERT( sizeof( digest_data ) % sizeof( mining_difficulty_t ) == 0, "Total hash type size must be divisible by the mining difficulty type" );

    using iter_ptr_t = const mining_difficulty_t*;

    iter_ptr_t _begin = reinterpret_cast< iter_ptr_t >( digest_data.begin() ),
               _end   = reinterpret_cast< iter_ptr_t >( digest_data.end() );

    while( _begin != _end )
      if( *_begin++ < mining_difficulty )
        return false;

    return true;
  }

  void block::mine( uint32_t mining_difficulty )
  {
    while ( nonce != uint64_t(-1 ) )
    {
      const auto now = std::chrono::system_clock::now();
      timestamp = std::chrono::duration_cast< std::chrono::seconds >( now.time_since_epoch() ).count();
      if ( is_valid_mine( mining_difficulty ) ) return;
      ++nonce;
    }
    CPPTOOLS_ASSERT( false, "Nonce out of range" );
  }

  void block::validate( uint32_t mining_difficulty )const
  {
    CPPTOOLS_ASSERT( is_valid_mine( mining_difficulty ), "Mining difficulty not satisfied" );
    CPPTOOLS_ASSERT( get_difficulty() <= TAC_MAX_CHAIN_DIFFICULTY, "Mining difficulty exceeded" );
    CPPTOOLS_ASSERT( transactions.size() <= TAC_MAX_TRANSACTIONS_IN_BLOCK, "Number of transactions in block exceeded" );
    for( const auto& trx : transactions )
      trx.validate( timestamp );
  }

} } } // tac::chain::block
