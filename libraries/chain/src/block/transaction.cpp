#include <tac/chain/block/transaction.hpp>
#include <tac/chain/block/operations.hpp>

#include <tac/chain/types.hpp>
#include <tac/chain/config.hpp>

#include <cpptools/exception.hpp>

#include <cpptools/streamable.hpp>

#include <cassert>

namespace tac { namespace chain { namespace block {

  namespace
  {
    using cpptools::SHA256::encoder;
  }

  hash_t transaction::digest()const
  {
    encoder sha;
    cpptools::pack( sha, *this );
    return hash_t{ sha.digest(), 32 };
  }

  void transaction::validate( uint64_t timestamp )const
  {
    CPPTOOLS_ASSERT( timestamp <= expiration, "Transaction expired" );
    CPPTOOLS_ASSERT( expiration < timestamp + TAC_MAX_TRANSACTION_EXPIRATION, "Transaction expiration must not be set to value greater than 1 hour" );
    for( const auto& op : operations )
      std::visit( []( auto&& actual_op ) {
        actual_op.validate(); // const
      }, op );
  }

} } } // tac::chain::block
