#include <tac/chain/block/operations.hpp>

#include <tac/chain/config.hpp>

#include <cpptools/exception.hpp>

#include <cassert>

namespace tac { namespace chain { namespace block {

  void transfer_operation::validate()const
  {
    CPPTOOLS_ASSERT( amount.amount > 0, "Cannot transfer a null amount amount" );
    CPPTOOLS_ASSERT( memo.size() < TAC_MAX_MEMO_SIZE, "Memo is too large. Max size is 2048 bytes" );
  }

} } } // tac::chain::block
