#include <tac/chain/blockchain.hpp>

#include <tac/chain/block/block.hpp>

#include <tac/chain/types.hpp>

#include <tac/chain/config.hpp>

#include <cpptools/exception.hpp>

#include <cassert>

namespace tac { namespace chain {

  blockchain::blockchain()
    : mining_difficulty( TAC_INITIAL_MINING_DIFFICULTY )
  {
    // TODO: Just for testing purposes before p2p network
    block_t _b;
    _b.previous = hash_t{};
    _b.mine( mining_difficulty );
    pending.push_back( _b );
  }

  void blockchain::add( const block_t& _b )
  {
    pending.push_back( _b );
  }

  void blockchain::mine()
  {
    if ( pending.size() == 0 ) CPPTOOLS_ASSERT( false, "There are no pending transactions in the blockchain" );
    auto& _b = pending.front();
    if( chain.size() )
      CPPTOOLS_ASSERT( chain.back().digest() == _b.previous, "Work not for the last block" );
    _b.mine( mining_difficulty );
    _b.validate( mining_difficulty );
    chain.push_back( _b );
    mining_difficulty = _b.get_difficulty();
    pending.pop_front();
  }

  void blockchain::validate()const
  {
    mining_difficulty_t current_diff = TAC_INITIAL_MINING_DIFFICULTY;
    uint64_t prev_timestamp = 0;
    hash_t prev_hash{};
    for( const auto& _b : chain )
    {
      CPPTOOLS_ASSERT( prev_hash == _b.previous, "Block hash mismatch" );
      CPPTOOLS_ASSERT( _b.timestamp - prev_timestamp >= TAC_MIN_BLOCK_PRODUCTION_GAP, "Minimum block production gap not satisfied" );
      _b.validate( current_diff );

      // Apply settings for the next block
      prev_hash = _b.digest();
      current_diff = _b.get_difficulty();
      prev_timestamp = _b.timestamp;
    }
  }

  const block_t& blockchain::get_block( size_t index )const
  {
    CPPTOOLS_ASSERT( index && index <= get_chain_size(), "Block index out of bounds" );
    return chain.at( index - 1 );
  }

  inline size_t blockchain::get_chain_size()const
  { return chain.size(); }

  inline size_t blockchain::get_pending_size()const
  { return pending.size(); }

  inline mining_difficulty_t blockchain::get_mining_difficulty()const
  { return mining_difficulty; }

} } // tac::chain
