#include <tac/chain/asset.hpp>

#include <cpptools/math.hpp>

#include <cpptools/exception.hpp>

#include <cassert>
#include <cstring>

#include <string>

namespace tac { namespace chain {

  namespace
  {
    namespace math = cpptools::__detail::__math;
  }

  symbol::symbol( uint32_t asset_num, uint32_t precision )
  {
    CPPTOOLS_ASSERT( asset_num <= 0xffffff, "Asset number should be in range 0-16777215" );
    CPPTOOLS_ASSERT( precision && precision <= 0xff, "Precision should be in range 1-255" );
    memcpy( &this->asset_num, reinterpret_cast< const math::hex_unit_t* >( &asset_num ), 3 );
    memcpy( reinterpret_cast< math::hex_unit_t* >( &this->asset_num )+3, reinterpret_cast< const math::hex_unit_t* >( &precision ), 1 );
  }

  std::string symbol::to_nai()const
  {
    CPPTOOLS_ASSERT( asset_num, "Asset num not initialized" );
    // TODO: Damm algorithm
    return "@@" + math::to_hex( reinterpret_cast< const math::hex_unit_t* >( &asset_num ), sizeof(asset_num) );
  }

  symbol symbol::from_nai( const std::string& nai_str )
  {
    CPPTOOLS_ASSERT( nai_str[ 0 ] == '@' && nai_str[ 1 ] == '@', "Not a NAI string" );
    symbol _s;
    math::hex_unit_t buff[sizeof(asset_num)] = {};
    math::from_hex( nai_str.c_str()+2, buff, sizeof(asset_num) );
    memcpy( &_s.asset_num, buff, sizeof(asset_num) );
    return _s;
  }

} } // tac::chain
