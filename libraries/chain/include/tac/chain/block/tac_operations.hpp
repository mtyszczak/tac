#pragma once

#include <tac/chain/block/base_operations.hpp>

#include <tac/chain/types.hpp>
#include <tac/chain/asset.hpp>

#include <cpptools/streamable.hpp>

#include <string>

namespace tac { namespace chain { namespace block {

  using string = std::string;

  /**
    * @ingroup operations
    *
    * @brief Transfers any liquid asset (nonvesting) from one account to another.
    */
  struct transfer_operation : public base_operation
  {
    /// Account to transfer asset from
    addr_t  from;
    /// Account to transfer asset to
    addr_t  to;
    /// The amount of asset to transfer from @ref from to @ref to
    asset_t amount;

    /// The memo is plain-text, any encryption on the memo is up to a higher level protocol.
    string  memo;

    void validate()const;
  };

} } } // tac::chain::block

namespace cpptools {

  namespace
  {
    namespace tcb = tac::chain::block;
  }

  template< OutputStreamable StreamType > inline void pack( StreamType& s, const tcb::transfer_operation& data )
    {
      pack( s, data.from );
      pack( s, data.to );
      pack( s, data.amount );
      pack( s, data.memo );
    }

} // cpptools
