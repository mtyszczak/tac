#pragma once

#include <tac/chain/block/operations.hpp>
#include <tac/chain/block/extensions/extensions.hpp>

#include <tac/chain/types.hpp>

#include <cpptools/streamable.hpp>

#include <vector>

namespace tac { namespace chain { namespace block {

  using namespace extensions;

  struct transaction
  {
    uint64_t                      expiration;

    std::vector< operation_t >    operations;

    extensions_t                  extensions;

    // TODO: Create documentation
    hash_t digest()const;
    void   validate( uint64_t timestamp )const;
  };

  struct signed_transaction : public transaction
  {
    // std::vector< signature_t >    signatures;

    // void sign
  };

} } } // tac::chain::block

namespace cpptools {

  namespace
  {
    namespace tcb = tac::chain::block;
  }

  template< OutputStreamable StreamType > inline void pack( StreamType& s, const tcb::transaction& data )
    {
      pack( s, data.expiration );
      pack( s, data.operations );
      pack( s, data.extensions );
    }

  template< OutputStreamable StreamType > inline void pack( StreamType& s, const tcb::signed_transaction& data )
    { pack( s, *( dynamic_cast< const tcb::transaction* >( &data ) ) ); }

} // cpptools
