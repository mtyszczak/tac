#pragma once

#include <cpptools/streamable.hpp>

#include <cstdint>

namespace tac { namespace chain { namespace block { namespace extensions {

  struct created_at_extension
  {
    uint64_t time_since_epoch;

    void use_now();
  };

} } } } // tac::chain::block::extensions

// TODO: std::hash on all types

namespace cpptools {

  namespace
  {
    namespace tcbe = tac::chain::block::extensions;
  }

  template< OutputStreamable StreamType > inline void pack( StreamType& s, const tcbe::created_at_extension& data )
    { pack( s, data.time_since_epoch ); }

} // cpptools
