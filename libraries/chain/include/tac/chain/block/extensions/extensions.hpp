#pragma once

#include <cpptools/streamable.hpp>

#include <tac/chain/block/extensions/base_extensions.hpp>

namespace tac { namespace chain { namespace block { namespace extensions {

  typedef std::variant<
      created_at_extension
    >                                future_extensions_t;

  typedef std::vector< future_extensions_t > extensions_t;

} } } } // tac::chain::block::extensions
