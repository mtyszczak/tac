#pragma once

#include <tac/chain/block/transaction.hpp>

#include <tac/chain/types.hpp>

#include <cpptools/streamable.hpp>

#include <cstdint>

namespace tac { namespace chain { namespace block {

  class block
  {
  public:
    uint64_t nonce = 0;
    uint64_t timestamp = 0;
    hash_t   previous;

    std::vector< signed_transaction > transactions;

    hash_t digest()const;

    bool is_valid_mine( mining_difficulty_t mining_difficulty )const;

    void mine( mining_difficulty_t mining_difficulty );

    mining_difficulty_t get_difficulty()const;

    void validate( mining_difficulty_t mining_difficulty )const; // TODO: Save mining_difficulty in the gpo
  };

  using block_t = block;

} } } // tac::chain::block

namespace cpptools {

  namespace
  {
    namespace tcb = tac::chain::block;
  }

  template< OutputStreamable StreamType > inline void pack( StreamType& s, const tcb::block& data )
    {
      pack( s, data.nonce );
      pack( s, data.timestamp );
      pack( s, data.previous );
      pack( s, data.transactions );
    }

} // cpptools
