#pragma once

#include <tac/chain/block/tac_operations.hpp>

#include <variant>

namespace tac { namespace chain { namespace block {

  typedef std::variant<

    transfer_operation

  > operation_t;

  // TODO: Create struct operation_name_visitor or create reflect for json serialization

} } } // tac::chain::block
