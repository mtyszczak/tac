#pragma once

#include <cpptools/exception.hpp>

namespace tac { namespace chain { namespace block {

  struct base_operation
  {
    consteval inline bool is_virtual()const { return false; }
    void validate()const {}
  };

  struct virtual_operation : public base_operation
  {
    consteval inline bool is_virtual()const { return true; }
    void validate()const { CPPTOOLS_ASSERT( false, "This is a virtual operation" ); }
  };

} } } // tac::chain::block
