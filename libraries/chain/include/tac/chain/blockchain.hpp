#pragma once

#include <tac/chain/block/block.hpp>

#include <tac/chain/types.hpp>

#include <string>
#include <deque>

namespace tac { namespace chain {

  namespace
  {
    using block::block_t;
  }

  class blockchain
  {
  private:
    std::deque< block_t > chain; // blockchain holder
    std::deque< block_t > pending; // pending blocks to mine
    mining_difficulty_t mining_difficulty;

  public:
    explicit blockchain();

    void add( const block_t& _b );

    void mine();

    void validate()const;

    const block_t& get_block( size_t index )const;

    inline size_t get_chain_size()const;
    inline size_t get_pending_size()const;

    inline mining_difficulty_t get_mining_difficulty()const;
  };

} } // tac::chain
