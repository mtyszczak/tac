#pragma once

#include <cpptools/streamable.hpp>

#include <string>

#include <cstdint>
#include <cstring>

namespace tac { namespace chain {

  class symbol
  {
  public:
    constexpr symbol() : asset_num(0) {}
    explicit symbol( uint32_t asset_num, uint32_t precision );

    std::string to_nai()const;
    static symbol from_nai( const std::string& nai_str ); // TODO: Work on asset (asset_num etc.)

    inline operator std::string()const { return to_nai(); }

    uint32_t asset_num;
  };

  typedef uint64_t amount_t;
  typedef symbol   symbol_t;

  struct asset
  {
    amount_t amount;
    symbol_t asset_symbol;

    inline operator std::string()const { return std::to_string(amount) + asset_symbol.to_nai(); }
  };

  typedef asset    asset_t;

} } // tac::chain

inline bool operator ==( const tac::chain::symbol& a, const tac::chain::symbol& b )
  { return a.asset_num == b.asset_num; }

inline auto operator <=>( const tac::chain::symbol& a, const tac::chain::symbol& b )
  { return a.asset_num <=> b.asset_num; }

inline bool operator ==( const tac::chain::asset& a, const tac::chain::asset& b )
  { return memcmp( &a, &b, sizeof(a) ) == 0; }

inline auto operator <=>( const tac::chain::asset& a, const tac::chain::asset& b )
  { return memcmp( &a, &b, sizeof(a) ); }

inline tac::chain::asset& operator +=( tac::chain::asset& a, const tac::chain::asset& b )
  { a.amount += b.amount; return a; }

inline tac::chain::asset& operator -=( tac::chain::asset& a, const tac::chain::asset& b )
  { a.amount -= b.amount; return a; }

inline tac::chain::asset& operator +=( tac::chain::asset& a, tac::chain::amount_t b )
  { a.amount += b; return a; }

inline tac::chain::asset& operator -=( tac::chain::asset& a, tac::chain::amount_t b )
  { a.amount -= b; return a; }

inline tac::chain::asset operator +( tac::chain::asset a, const tac::chain::asset& b )
  { a.amount += b.amount; return a; }

inline tac::chain::asset operator -( tac::chain::asset a, const tac::chain::asset& b )
  { a.amount -= b.amount; return a; }

inline tac::chain::asset operator +( tac::chain::amount_t a, tac::chain::asset b )
  { b.amount += a; return b; }

inline tac::chain::asset operator -( tac::chain::amount_t a, tac::chain::asset b )
  { b.amount -= a; return b; }

inline tac::chain::asset operator +( tac::chain::asset a, tac::chain::amount_t b )
  { a.amount += b; return a; }

inline tac::chain::asset operator -( tac::chain::asset a, tac::chain::amount_t b )
  { a.amount -= b; return a; }

namespace std
{
  template<>
  struct hash< tac::chain::symbol >
  {
    size_t operator()( const tac::chain::symbol& __h ) const noexcept
    { return ( __h.asset_num + 17 ) * 17; }
  };

  template<>
  struct hash< tac::chain::asset >
  {
    size_t operator()( const tac::chain::asset& __h ) const noexcept
    { return ( __h.amount + 13 ) * 31 + hash< tac::chain::symbol >()( __h.asset_symbol ); }
  };
} // std

namespace cpptools {

  namespace
  {
    namespace tc = tac::chain;
  }

  template< OutputStreamable StreamType > inline void pack( StreamType& s, const tc::symbol& data )
    {
      pack( s, data.asset_num );
    }

  template< OutputStreamable StreamType > inline void pack( StreamType& s, const tc::asset& data )
    {
      pack( s, data.amount );
      pack( s, data.asset_symbol );
    }

} // cpptools
