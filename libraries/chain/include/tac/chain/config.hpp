#pragma once

#include <cstdint>
#include <tac/chain/types.hpp>

// Transactions related configuration:
#define TAC_MAX_TRANSACTION_EXPIRATION ( 60*60 ) // 1 hour

#define TAC_MAX_TRANSACTIONS_IN_BLOCK  ( uint8_t(-1) / 2 ) // 255

// Chain related configuration:
#define TAC_MAX_CHAIN_DIFFICULTY       ( mining_difficulty_t(-2) / 2 ) // 2147483647
#define TAC_INITIAL_MINING_DIFFICULTY  ( TAC_MAX_CHAIN_DIFFICULTY / ( TAC_MAX_TRANSACTIONS_IN_BLOCK / 2 ) ) // ~16909320

// Block related configuration:
#define TAC_MAX_BLOCK_SIZE             ( TAC_INITIAL_MINING_DIFFICULTY / 1024 ) // ~2097151 (in bytes) ~= 2 MiB
#define TAC_MAX_BLOCK_SIZE_MULTIPLIER  ( 2 * 1024 ) // 2048 to achieve TAC_MAX_CHAIN_DIFFICULTY
#define TAC_MIN_BLOCK_PRODUCTION_GAP   3 // 3 seconds

// Operations related configuration:
#define TAC_MAX_MEMO_SIZE              2048
