#pragma once

#include <cpptools/hash.hpp>

namespace tac { namespace chain {

  typedef cpptools::hash< uint64_t, 2 > addr_t; // Address type
  typedef cpptools::hash< uint8_t, 32 > hash_t; // Block hash and trx id type

  typedef uint32_t mining_difficulty_t;

} } // tac::chain
