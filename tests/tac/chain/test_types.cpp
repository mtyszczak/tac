#include <tac/chain/types.hpp>

#include <gtest/gtest.h>

namespace
{

  namespace
  {
    namespace tc = tac::chain;
  }

  TEST( test_types, address_type_length )
  {
    EXPECT_EQ( sizeof( tc::addr_t ), 16 );
  }

  TEST( test_types, hash_type_length )
  {
    EXPECT_EQ( sizeof( tc::hash_t ), 32 );
  }

}